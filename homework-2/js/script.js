/**
 * Класс, объекты которого описывают параметры гамбургера. 
 * 
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
class Hamburger {

	constructor(size, stuffing) {
		try {
			if (size === undefined) {
				throw new HamburgerException('Error! Size is undefined')
			} 
			if (size.type !== 'size') {
				throw new HamburgerException('Error! Size is not size')
			} 
			if (stuffing.type !== 'stuffing') {
				throw new HamburgerException('Error! Stuffing is not stuffing')
			}
			this.size = size;
			this.stuffing = stuffing;
			this.topping = [];
		} catch (err) {
			console.log(err.message)

		}
	}

	set addTopping(topping) {
		try {
			if (topping === undefined) {
				throw new HamburgerException('Topping is undefined!')
			}
			if (topping.type !== 'topping') {
				throw new HamburgerException('Is not topping!')
			}
			if (this.topping.includes(topping)) {
				throw new HamburgerException('This topping already here!')
			}
			this.topping.push(topping);
		} catch (err) {
			console.log(err.message)
		}
	}
	set removeTopping(topping) {
		//фильтруем
		this.topping = this.topping.filter(function (item) {
			return item !== topping
		});

		//поиск по индексу
		// let toppingIndex = this.topping.indexOf(topping);
		// if (toppingIndex > -1) {
		// 	this.topping.splice(toppingIndex, 1)
		// } else {
		// console.log('Нэма такого топыпна')	
	}

	get getToppings() {
		return this.topping
	}

	get getSize() {
		return this.size.name;
	}
	get getStuffing() {
		return this.stuffing.name
	}
	
	calculatePrice = function () {
		let price = this.size.price + this.stuffing.price
		this.topping.forEach(function (el) {
			price += el.price;
		})
		return price
	}
	calculateCalories = function () {
		let cal = this.size.cal + this.stuffing.cal
		this.topping.forEach(function (el) {
			cal += el.cal
		})
		return cal
	}

}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
	name: 'SIZE_SMALL',
	price: 50,
	cal: 20,
	type: 'size'
}
Hamburger.SIZE_LARGE = {
	name: 'SIZE_LARGE',
	price: 100,
	cal: 40,
	type: 'size'
}
Hamburger.STUFFING_CHEESE = {
	name: 'STUFFING_CHEESE',
	price: 10,
	cal: 20,
	type: 'stuffing'
}
Hamburger.STUFFING_SALAD = {
	name: 'STUFFING_SALAD',
	price: 20,
	cal: 5,
	type: 'stuffing'
}
Hamburger.STUFFING_POTATO = {
	name: 'STUFFING_POTATO',
	price: 15,
	cal: 10,
	type: 'stuffing'
}
Hamburger.TOPPING_MAYO = {
	name: 'TOPPING_MAYO',
	price: 20,
	cal: 5,
	type: 'topping'
}
Hamburger.TOPPING_SPICE = {
	name: 'TOPPING_SPICE',
	price: 15,
	cal: 0,
	type: 'topping'
}


function HamburgerException(err) {
	this.message = err
}


let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
// let cheeseburger = new Hamburger( Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

burger.addTopping = Hamburger.TOPPING_MAYO
burger.addTopping = Hamburger.TOPPING_SPICE
burger.removeTopping = Hamburger.TOPPING_MAYO
//
//console.log(burger.getSize());
//console.log(burger.getStuffing());
console.log(burger.calculatePrice());
console.log(burger.getToppings);
console.log(burger.calculateCalories());
