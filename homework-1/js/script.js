/**
 * Класс, объекты которого описывают параметры гамбургера. 
 * 
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
	try {
		if (size === undefined) {
			throw new HamburgerException('Error! Size is undefined')
		} else if (size.type !== 'size') {
			throw new HamburgerException('Error! Size is not size')
		} else if (stuffing.type !== 'stuffing') {
			throw new HamburgerException('Error! Stuffing is not stuffing')
		}
		this.size = size;
		this.stuffing = stuffing;
		this.topping = [];
	} catch (err) {
		console.log(err.message)

	}

}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
	name: 'SIZE_SMALL',
	price: 50,
	cal: 20,
	type: 'size'
}
Hamburger.SIZE_LARGE = {
	name: 'SIZE_LARGE',
	price: 100,
	cal: 40,
	type: 'size'
}
Hamburger.STUFFING_CHEESE = {
	name: 'STUFFING_CHEESE',
	price: 10,
	cal: 20,
	type: 'stuffing'
}
Hamburger.STUFFING_SALAD = {
	name: 'STUFFING_SALAD',
	price: 20,
	cal: 5,
	type: 'stuffing'
}
Hamburger.STUFFING_POTATO = {
	name: 'STUFFING_POTATO',
	price: 15,
	cal: 10,
	type: 'stuffing'
}
Hamburger.TOPPING_MAYO = {
	name: 'TOPPING_MAYO',
	price: 20,
	cal: 5,
	type: 'topping'
}
Hamburger.TOPPING_SPICE = {
	name: 'TOPPING_SPICE',
	price: 15,
	cal: 0,
	type: 'topping'
}

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 * 
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
	try {
		if (topping === undefined) {
			throw new HamburgerException('Topping is undefined!')
		}
		if (topping.type !== 'topping') {
			throw new HamburgerException('Is not topping!')
		}
		if (this.topping.includes(topping)) {
			throw new HamburgerException('This topping already here!')
		}
		this.topping.push(topping);
	} catch (err) {
		console.log(err.message)
	}
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {


	//фильтруем
	this.topping = this.topping.filter(function (item) {
		return item !== topping
	});
	
	//поиск по индексу
	// let toppingIndex = this.topping.indexOf(topping);
	// if (toppingIndex > -1) {
	// 	this.topping.splice(toppingIndex, 1)
	// } else {
	// console.log('Нэма такого топыпна')	
}


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
	return this.topping
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
	return this.size.name;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
	return this.stuffing.name
}

Hamburger.prototype.calculatePrice = function () {
	price = this.size.price + this.stuffing.price
	this.topping.forEach(function (el) {
		price += el.price;
	})
	return price
}

Hamburger.prototype.calculateCalories = function () {
	cal = this.size.cal + this.stuffing.cal
	this.topping.forEach(function(el){
		cal+=el.cal
	})
	return cal
}

///**
// * Представляет информацию об ошибке в ходе работы с гамбургером. 
// * Подробности хранятся в свойстве message.
// * @constructor 
// */
function HamburgerException(err) {
	this.message = err
}


let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
// let cheeseburger = new Hamburger( Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

burger.addTopping(Hamburger.TOPPING_MAYO)
burger.addTopping(Hamburger.TOPPING_SPICE)

//burger.removeTopping(Hamburger.TOPPING_MAYO)
//
//console.log(burger.getSize());
//console.log(burger.getStuffing());
console.log(burger.calculatePrice());
console.log(burger.getToppings());

//console.log(burger.calculateCalories());
